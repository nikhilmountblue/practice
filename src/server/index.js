//Libraries that are imported to this Code
const csv = require ('csvtojson');
const fs = require ('fs');

//Csv data and its path 
const  deliveriesFilePath  =  '../data/deliveries.csv';
const  matchesFilePath  =  '../data/matches.csv';


csv()
.fromFile ( matchesFilePath )
.then( ( matchData ) => { 
    
    ipl_Match_Played_Per_Year(matchData);
    matches_Won_By_Each_Team_Per_Year(matchData);
    
    csv()
    .fromFile ( deliveriesFilePath )
    .then( ( deliveriesData ) => { 

        extra_Runs_Conceded_By_Each_Team_In_The_Year( deliveriesData , id =startAndEndID( matchData , year =2016) ,year =2016);
        top_10_Economical_Bowler_In_The_Year( deliveriesData , id =startAndEndID( matchData , year =2015) ,year =2015);

    });
});

//Bellow function used to extract Match-Id of a particular Year 
function startAndEndID( matchData, year ){

    let start ;
    let end ;
    
    for(let Index = 0; Index < matchData.length; Index++){
    
        if( parseInt( matchData[ Index ].season)===year ){
    
            if( !start ){
    
                start=Index+1;
            }
      
            end=Index+1;
        }
    }
    return start+" "+end;
}


// Bellow function used to formating the output data 
function output( folder_Name,answer )
{
    let str=JSON.stringify(answer , null , " ") 
    fs.writeFile('../output1/'+folder_Name+'.json', str, function (err) { 
        if (err) {

        return console.log( "An Error Occured while writing a file" );
        } 
    });
}


// Bellow function is implemetend to extract Number of Ipl match played per year
function ipl_Match_Played_Per_Year( matchData )
{
    let answer={};
    for(let Index = 0;  Index < matchData.length;  Index++){
        
        if(!answer[ matchData[ Index ].season ]){
        
            answer[ matchData[ Index ].season ] =1; 
        
        } else {

            answer[ matchData[ Index ].season ] +=1;
 
        }
    }
    output( 'Ipl_Match_Played_Per_Year' , answer );
}


// Bellow function is implemetend to extract Number of match won by each team per year
function matches_Won_By_Each_Team_Per_Year( matchData )
{
    let answer = {};
    for(let Index = 0;  Index < matchData.length;  Index++){
        
        if(!answer[ matchData[Index].season ]){
        
            answer[ matchData[Index].season ] = {};
            answer[ matchData[Index].season ][ matchData[Index].winner ] = 1;
        }
        else
        {
            if(!answer[ matchData[Index].season ][ matchData[Index].winner]){

                answer[ matchData[Index].season ][ matchData[Index].winner] = 1;
            } else {

                answer[ matchData[Index].season ][ matchData[Index].winner] += 1;
            }
        }
    }
    output( 'Matches_Won_By_Each_Team_Per_Year' , answer );
}


// Bellow function is implemetend to extract extra runs conceded by each team in that year
function extra_Runs_Conceded_By_Each_Team_In_The_Year( deliveriesData , id , year ){
    
    let idValue = id.split(" ");
    let answer = {};

    for(let Index =0; Index < deliveriesData.length; Index++){
        
        if (parseInt( deliveriesData[ Index ].match_id ) >= parseInt( idValue[ 0 ] ) && parseInt( deliveriesData[ Index ].match_id ) <= parseInt( idValue[ 1 ]) ){
            
            if(answer[ deliveriesData[ Index ].bowling_team ] !== undefined){

                answer[ deliveriesData[ Index ].bowling_team ]+=parseInt( deliveriesData[ Index ].extra_runs );
            
            } else {
            
                answer[ deliveriesData[ Index ].bowling_team ]=parseInt( deliveriesData[ Index ].extra_runs );
            
            }
        }
    }
    output( 'Extra_Runs_Conceded_By_Each_Team_In_The_Year_'+year , answer );
}


// Bellow function is implemetend to extract top 10 economical bowler in that year
function top_10_Economical_Bowler_In_The_Year( deliveriesData, id , year ){
    
    let idValue = id.split(" ");
    let answer = {}

    for(let Index =0; Index < deliveriesData.length; Index++){
       
        if (parseInt( deliveriesData[ Index ].match_id ) >= parseInt( idValue[ 0 ] ) && parseInt( deliveriesData[ Index ].match_id ) <= parseInt( idValue[ 1 ]) ){

            if(answer[ deliveriesData [ Index ].bowler ] === undefined ){

                    answer [ deliveriesData [Index].bowler ] = {
                    no_of_balls : 1,
                    score : parseInt( deliveriesData [ Index ].total_runs ) }
            } else {

                if(parseInt( deliveriesData [ Index ].wide_runs ) ===0 || parseInt( deliveriesData [ Index ].noball_runs ==0 )){

                    answer [ deliveriesData [ Index ].bowler ].no_of_balls += 1
                    answer [ deliveriesData [ Index ].bowler ].score += parseInt( deliveriesData [ Index ].total_runs ) 
                
                } else{
                
                    answer [ deliveriesData [ Index ].bowler ].no_of_balls += 0
                    answer [ deliveriesData [ Index ].bowler ].score += parseInt( deliveriesData [ Index ].total_runs )
                }
            }
        }
    }

    let allValue = {};
    let top10 = {};
    //Calculation of economic of the bowlers
    for( let key in answer )
    allValue [ key ] =( answer [ key ].score / ( answer [ key ].no_of_balls ) )*6;

    // Sorting the object using .sort 
    let key = Object.keys( allValue )
    key.sort( function ( value1, value2 ) 
        { return allValue [ value1 ] - allValue[ value2 ] });

    
    for( let Index =0; Index < 10; Index++ )
    top10[ key[ Index ] ] = allValue[ key[ Index ] ]

    output('Top_10_Economical_Bowler_In_The_Year_'+year,top10)
}